module mixings

! This module contains SCF mixing algorithms.

use types, only: dp
use constants, only: pi
use dft_data, only: dft_data_t
use ode1d, only: integrate
implicit none
private
public mixing_anderson

contains

function mixing_anderson(R, x0, max_iter, energy_crit, d, alpha, eps)
! Finds "x" so that R(x) = 0, uses x0 as the initial estimate
real(dp), intent(in) :: x0(:)
integer, intent(in) :: max_iter
logical, intent(in) :: energy_crit
type(dft_data_t), intent(inout) :: d ! Data passed to "F"
real(dp), intent(in) :: alpha
real(dp), intent(in) :: eps
real(dp) :: mixing_anderson(size(x0))
interface
    function R(x, i, d)
    use types
    use dft_data
    implicit none
    real(dp), intent(in) :: x(:) ! "x"
    integer, intent(in) :: i ! iteration #
    type(dft_data_t), intent(inout) :: d ! F's data
    real(dp) :: R(size(x))
    end function
end interface

real(dp), dimension(size(x0)) :: x_i, x1_i, R_i, R1_i, delta_R, delta_x
real(dp) :: beta
real(dp) :: sn, sd
real(dp) :: ks_energies(size(d%ks_energies))
real(dp) :: x_i_norm, R_i_norm
real(dp) :: err_old, err, L2_err
integer :: i
x_i = x0
if (energy_crit) then
    ks_energies = d%ks_energies
    err_old = 1e12_dp
end if
do i = 1, max_iter
    R_i = R(x_i, i, d)
    if (energy_crit) then
        ! L2 norm of the "input" potential:
        x_i_norm = sqrt(4*pi*integrate(d%Rp, x_i**2 * d%R**2))
        ! L2 norm of the "output-input" potential:
        R_i_norm = sqrt(4*pi*integrate(d%Rp, R_i**2 * d%R**2))
        if (x_i_norm < 1e-12_dp) x_i_norm = 1e-12_dp
        L2_err = R_i_norm / x_i_norm
        err = maxval(abs(ks_energies - d%ks_energies))
        !print *, i, L2_err, err
        ! Do at least 3 iterations
        if (i >= 3 .and. L2_err < 5e-5_dp) then
            if (err < eps .and. err_old < eps) then
                mixing_anderson = x_i
                return
            end if
        end if
        ks_energies = d%ks_energies
        err_old = err
    end if

    if (i > 1) then
        delta_x = x_i - x1_i
        delta_R = R_i - R1_i
    end if
    x1_i = x_i
    R1_i = R_i
    x_i = x_i + alpha * R_i
    if (i > 1) then
        sn = sum(R_i * delta_R * d%R**2)
        sd = sum(delta_R**2 * d%R**2)
        beta = sn / sd
        x_i = x_i - beta * (delta_x + alpha * delta_R)
    end if
end do
mixing_anderson = x_i
if (energy_crit) error stop "SCF didn't converge"
end function

end module
