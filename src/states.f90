module states

! This module lists nonrelativistic and relativistic atomic configurations.
! The nonrelativistic configurations are the same as at NIST [1] and are simply
! hardcoded in the subroutine for each atom. The relativistic configuration is
! then calculated from the nonrelativistic by splitting the occupancy according
! to the degeneracy (see the soubroutine for more info).
!
! [1] "Local-density-functional calculations of the energy of atoms,"
! S. Kotochigova, Z.H. Levine, E.L. Shirley, M.D. Stiles, and C.W. Clark,
! Phys. Rev.  A 55, 191-199 (1997).

use types, only: dp
implicit none
private
public get_atomic_states_nonrel

contains

subroutine get_atomic_states_nonrel(Z, no, lo, fo)
! Returns all electron states of the form (n, l, f) for a given Z
integer, intent(in) :: Z ! atomic number
integer, allocatable, intent(out) :: no(:), lo(:) ! quantum numbers "n" and "l"
real(dp), allocatable, intent(out) :: fo(:) ! occupancy of the (n, l) state
! Note: sum(fo) == Z

integer :: n

select case (Z)

    case (92)
        n = 18
        allocate(no(n), lo(n), fo(n))
        no = [ 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 7 ]
        lo = [ 0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 0 ]
        fo = [ 2, 2, 6, 2, 6, 10, 2, 6, 10, 14, 2, 6, 10, 3, 2, 6, 1, 2 ]

    case default
        print *, "Z =", Z
        error stop "Z not supported."
end select
end subroutine

end module
